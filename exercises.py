"""
Exercise no. 7 is run by: python exercises.py

"""
import random
import logging
from datetime import datetime, timedelta
from collections import defaultdict
from cmd import Cmd
from itertools import product


def fizz_buzz():
    """
    Write a program that prints the numbers from 1 to 100.
    But for multiples of three print "Fizz" instead of the number
    and for multiples of five print "Buzz". For numbers that are
    multiples of both three and five print "FizzBuzz".

    """
    for x in range(1, 101):
        val = ''
        if not x % 3:
            val += 'Fizz'
        if not x % 5:
            val += 'Buzz'
        if not val:
            val = x
        print val


def db_conn():
    """
    Demonstrate in Python how you would connect to a MySQL database that
    uses the InnoDB storage engine and query for all records (with the
    following fields: name, age, role) from a table called 'dedsert_test', also
    provide an example of how you would write a record to the same table.

    """
    # a pseudo code. Import would go elsewhere.
    import pymysql
    db = pymysql.connect(user='user', passwd='password', db='some_db')
    cur = db.cursor()
    cur.execute('select name, age, role from dedsert_test')
    result = cur.fetchall()

    cur.execute('insert into dedsert_test '
                'set name="Michal", age=4, role="root"')


def next_draw_date(since=datetime.now()):
    """
    The Irish lottery draw takes place twice weekly on a Wednesday and
    a Saturday at 8pm. Write a function that calculates and returns
    the next valid draw date based on the current date and time and
    also on an optional supplied date.

    Args:
    since - since when calculate next draw date.

    """
    def get_draw_date(weekday):
        draw_day = since + timedelta(days=weekday - since.weekday())
        return draw_day.replace(hour=20, minute=0,
                                second=0, microsecond=0)

    def calc_current_week_draw(weekday):
        draw_date = get_draw_date(weekday)
        if (draw_date - since).total_seconds() < 0:
            return calc_current_week_draw(weekday + 7)
        return draw_date

    wed_draw_date = calc_current_week_draw(2)
    sat_draw_date =  calc_current_week_draw(5)
    if wed_draw_date < sat_draw_date:
        return wed_draw_date
    else:
        return sat_draw_date


def lru_cache(max_size=100):
    """
    Implement a least recently used (LRU) cache mechanism using
    a decorator and demonstrate it use in a small script.
    The LRU must be able to admit a 'max_size' parameter that
    by default has to be 100.

    In order to test this run: python lru_cache_test.py

    """
    def my_decorator(func):
        def wrapped_func(number):
            if number in c_val:
                logging.info('Cache hit for %s.', number)
                c_hit[number] += 1
                return c_val[number]

            if len(c_val) == max_size:
                logging.info('Cache overflow.')
                least_used_index = min(c_hit, key=lambda k: c_hit[k])
                logging.info('Removing %s', least_used_index)
                del c_val[least_used_index]
                del c_hit[least_used_index]

            res = func(number)
            c_val[number] = res
            c_hit[number] = 0
            return res
        c_hit = {}
        c_val = {}
        return wrapped_func
    return my_decorator


def find_anagram(word, words):
    """
    Write a function that accepts a word (string) and a list of words
    (list or tuple of strings) and return back a list with the valid
    anagrams for the word inside the given words list.

    """
    return [w for w in words if sorted(list(w)) == sorted(list(word))]


def find_int(int_list):
    """
    Write a function that finds an integer in a list of
    paired unsorted integers that appears only once. The algorithm time
    complexity can't be worst than O(n2).

    """
    d = defaultdict(int)
    for a, b in int_list:
        d[a] += 1
        d[b] += 1

    res = []
    for k, v in d.items():
        if v == 1:
            res.append(k)
    return res


class Deck(object):
    """
    Class implementing basic deck af cards.
    Used by CardsGame class.

    """
    SUITS = ['H', 'C', 'D', 'S']
    RANKS = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K']

    def __init__(self):
        super(Deck, self).__init__()
        self.deck = []

    def shuffle(self):
        self.deck = [suit + rank for suit, rank in
                     product(Deck.SUITS, Deck.RANKS)]
        random.shuffle(self.deck)
        return self.deck

    def deal(self, nr_players):
        if not self.deck:
            self.shuffle()
        return self._deal(nr_players)

    def _clear_deck(self):
        self.deck = []

    def _calc_player_hand(self, nr_players):
        return (len(self.deck) - (len(self.deck) % nr_players)) / nr_players

    def _deal_hand(self, nr_cards):
        players_hand = []
        for i in range(nr_cards):
            players_hand.append(self.deck.pop())
        return players_hand

    def _deal(self, nr_players):
        hands = []
        cards_to_deal = self._calc_player_hand(nr_players)
        for player in range(nr_players):
            hands.append(self._deal_hand(cards_to_deal))
        self._clear_deck()
        return hands


class CardsGame(Cmd, object):
    """
    Write a console application that simulates of a card game
    that mix up a standard deck of cards and then deal the cards
    to the players without using the same card more than once.

    """
    intro = """
    Cards Game.
    Type 'help' for help.
    Type 'exit' to close the game.

    """
    def __init__(self):
        super(CardsGame, self).__init__()
        self.deck = None
        self.players = 4

    def preloop(self):
        self.deck = Deck()

    def help_deal(self):
        print 'Deal cards to players'

    def do_deal(self, unused_s):
        print self.deck.deal(self.players)
        return

    def help_players(self):
        print 'Accepts number of players as an argument'
        print 'Example: Set game to 4 players'
        print 'players 4'

    def do_players(self, s):
        players = s.split()
        if len(players) > 1:
            print "*** invalid number of arguments"
            return
        try:
            self.players = int(players[0])
        except (ValueError, IndexError):
            print "*** argument should be number"
            return

    def help_exit(self):
        print 'Close game.'

    def do_exit(self, *args):
        return True


if __name__ == '__main__':
    cards_game = CardsGame()
    cards_game.cmdloop()
