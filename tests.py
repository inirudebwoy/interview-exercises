"""
Tests should be run with nose.

"""
from datetime import datetime
from itertools import chain

from exercises import next_draw_date, find_anagram, find_int, \
     Deck

from nose.tools import with_setup


def test_next_draw_date():
    """ Test for next_draw_date """
    assert next_draw_date(datetime(2014, 5, 21, 21)) == \
      datetime(2014, 5, 24, 20)
    assert next_draw_date(datetime(2014, 5, 31, 21)) == datetime(2014, 6, 4, 20)
    assert next_draw_date(datetime(2014, 5, 31, 19, 59, 59)) == \
      datetime(2014, 5, 31, 20)


def test_find_anagram():
    """ Test for find_anagram """
    assert find_anagram('mary', ('army', 'dog', 'murakami')) == ['army']
    assert find_anagram('mary', ('dog', 'cat')) == []
    assert find_anagram('mate', ('meat', 'meta', 'tame', 'team')) == \
      ['meat', 'meta', 'tame', 'team']


def test_find_int():
    """ Test for find_int """
    assert find_int([(1, 2), (3, 4), (1, 2), (3, 5), (5, 5)]) == [4]
    assert find_int([(4, 2), (3, 4), (1, 2), (3, 5), (4, 5)]) == [1]
    assert find_int([(1, 2), (1, 2), (1, 2), (1, 2), (5, 5), (3, 4)]) == [3, 4]


def setup_deck():
    """ Setup needed for Deck class testing """
    global deck
    deck = Deck()


def teardown_deck():
    """ Teardown needed for Deck class testing """
    del globals()['deck']


@with_setup(setup_deck, teardown_deck)
def test_init():
    """ Test for __init__ """
    assert deck.deck == []


@with_setup(setup_deck, teardown_deck)
def test_shuffle():
    """ Test for shuffle """
    d = deck.shuffle()
    assert len(d) == len(set(d))


@with_setup(setup_deck, teardown_deck)
def test_deal():
    """ Test for deal() """
    assert len(deck.deal(4)) == 4
    assert len(deck.deal(10)) == 10
    assert len(deck.deal(1)) == 1


@with_setup(setup_deck, teardown_deck)
def test_clear_deck():
    """ Test for _clear_deck """
    deck._clear_deck()
    assert deck.deck == []


@with_setup(setup_deck, teardown_deck)
def test_calc_player_hand():
    """ Test for _calc_player_hand """
    assert deck._calc_player_hand(4) == 0
    deck.shuffle()
    assert deck._calc_player_hand(4) == 13
    assert deck._calc_player_hand(10) == 5


@with_setup(setup_deck, teardown_deck)
def test_deal_hand():
    """ Test for _deal_hand """
    deck.shuffle()
    assert len(deck._deal_hand(10)) == 10
    assert len(deck._deal_hand(0)) == 0


@with_setup(setup_deck, teardown_deck)
def test__deal():
    """ Test for _deal """
    deck.shuffle()
    d = deck._deal(4)
    assert len(d) == 4
    # flatten list of lists, create set,
    # should equal to fresh deck, thus hands are unique
    assert len(set([hand for hand in chain.from_iterable(d)])) == \
      len(deck.shuffle())
