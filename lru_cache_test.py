import logging
import random

from exercises import lru_cache

@lru_cache()
def squared(number):
    """ Function demonstrating use of LRU cache """
    return number * number


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    for x in [int(200*random.random()) for i in xrange(1000)]:
        print squared(x)
